"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    class TransactionItem extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            this.belongsTo(models.Transaction, {
                targetKey: "Tid",
                foreignKey: "transaction_id",
                as: "transaction_items",
            });

            this.hasOne(models.Product, {
                sourceKey: "product_id",
                foreignKey: "id",
                as: "products",
            });
        }
    }
    TransactionItem.init(
        {
            TIid: DataTypes.STRING,
            transaction_id: DataTypes.STRING,
            product_id: DataTypes.INTEGER,
            product_name: DataTypes.STRING,
            price: DataTypes.INTEGER,
            quantity: DataTypes.INTEGER,
        },
        {
            sequelize,
            modelName: "TransactionItem",
        }
    );
    return TransactionItem;
};
