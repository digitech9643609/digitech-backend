"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    class Transaction extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here

            this.hasOne(models.TransactionItem, {
                sourceKey: "Tid",
                foreignKey: "transaction_id",
                as: "transaction_items",
            });
        }
    }
    Transaction.init(
        {
            Tid: DataTypes.STRING,
            total: DataTypes.INTEGER,
            status: DataTypes.STRING,
            customer_name: DataTypes.STRING,
            customer_email: DataTypes.STRING,
            snap_token: DataTypes.STRING,
            snap_redirect_url: DataTypes.STRING,
            payment_method: DataTypes.STRING,
        },
        {
            sequelize,
            modelName: "Transaction",
        }
    );
    return Transaction;
};
