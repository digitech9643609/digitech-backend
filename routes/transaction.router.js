const router = require("express").Router();
const transactionController = require("../controllers/transaction.controller");

router.post("/add", transactionController.createTransaction);

router.get("/by-id", transactionController.getTransactionById);

router.post("/notification", transactionController.transactionNotificationFromMidtrans);

module.exports = router;
