const express = require("express");
const router = express.Router();
const productRouter = require("./product.router");
const transactionRouter = require("./transaction.router");

/* GET home page. */
router.get("/", function (req, res, next) {
  res.render("index", { title: "Express" });
});

/* PRODUCT */
router.use("/product", productRouter);

/* TRANSACTION */
router.use("/transaction", transactionRouter);

module.exports = router;
