const router = require("express").Router();
const productController = require("../controllers/product.controller");

router.get("/all", productController.getAllProduct);

router.post("/add", productController.createProduct);

router.post("/add-multiple", productController.createNewMultipleProducts);

module.exports = router;
