const ProductServices = require("../services/product.services");
const TransactionService = require("../services/transaction.services");

module.exports = {
  productService: new ProductServices(),
  transactionService: new TransactionService(),
};
