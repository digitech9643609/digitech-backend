const { Transaction, TransactionItem, Product } = require("../models");
const { PENDING_PAYMENT } = require("../utils/constantVariables");
const generateString = require("../utils/stringGenerator");
const crypto = require("crypto");

class TransactionServices {
    constructor() {}

    async createTransaction({
        transaction_id,
        gross_amount,
        customer_name,
        customer_email,
        snap_token,
        snap_redirect_url,
    }) {
        try {
            const transactionCreated = await Transaction.create({
                Tid: transaction_id,
                total: gross_amount,
                status: "PENDING_PAYMENT",
                customer_name,
                customer_email,
                snap_token,
                snap_redirect_url,
            });

            return transactionCreated;
        } catch (error) {
            throw new Error("error at createTransaction : " + error.message);
        }
    }

    async createTransactionItems({ products, transaction_id }) {
        try {
            const transactionItems = [];

            products.map((product) =>
                transactionItems.push({
                    TIid: `TRX-ITEM-${generateString(10)}`,
                    transaction_id,
                    product_id: product.id,
                    product_name: product.name,
                    price: product.price,
                    quantity: product.quantity,
                })
            );

            const createdTransactionItems = await TransactionItem.bulkCreate(
                transactionItems
            );

            return createdTransactionItems;
        } catch (error) {
            throw new Error(
                "error at createTransactionItems : " + error.message
            );
        }
    }

    async getTransactionById({ transaction_id }) {
        try {
            const transactionData = await Transaction.findOne({
                where: {
                    Tid: transaction_id,
                },
                include: {
                    model: TransactionItem,
                    as: "transaction_items",
                    include: {
                        model: Product,
                        as: "products",
                        attributes: ["id", "name", "price"],
                    },
                },
            });

            return transactionData;
        } catch (error) {
            throw new Error("error at getTransactionById : " + error.message);
        }
    }

    async updateTransactionStatus({
        transaction_id,
        status,
        payment_method = null,
    }) {
        try {
            const updatedTransaction = await Transaction.update(
                { status, payment_method },
                {
                    where: {
                        Tid: transaction_id,
                    },
                }
            );

            return updatedTransaction;
        } catch (error) {
            throw new Error(
                "error at updateTransactionStatus : " + error.message
            );
        }
    }

    async updateStatusBasedOnMidtransResponse(transaction_id, midtransData) {
        try {
            const hash = crypto
                .createHash("sha512")
                .update(
                    `${transaction_id}${midtransData.status_code}${midtransData.gross_amount}${process.env.MIDTRANS_SERVER_KEY}`
                )
                .digest("hex");

            if (hash !== midtransData.signature_key) {
                return {
                    status: "error",
                    message: "invalid signature key",
                    hash,
                    midtransSignatureKey: midtransData.signature_key,
                };
            }

            let responseData = null;
            let transactionStatus = midtransData.transaction_status;
            let fraudStatus = midtransData.fraud_status;

            if (transactionStatus == "capture") {
                if (fraudStatus == "accept") {
                    const transaction = await this.updateTransactionStatus({
                        transaction_id,
                        status: "PAID",
                        payment_method: midtransData.payment_type,
                    });
                    responseData = transaction;
                }
            } else if (transactionStatus == "settlement") {
                const transaction = await this.updateTransactionStatus({
                    transaction_id,
                    status: "PAID",
                    payment_method: midtransData.payment_type,
                });
                responseData = transaction;
            } else if (
                transactionStatus == "cancel" ||
                transactionStatus == "deny" ||
                transactionStatus == "expire"
            ) {
                const transaction = await this.updateTransactionStatus({
                    transaction_id,
                    status: "CANCELED",
                });
                responseData = transaction;
            } else if (transactionStatus == "pending") {
                const transaction = await this.updateTransactionStatus({
                    transaction_id,
                    status: "PENDING_PAYMENT",
                });
                responseData = transaction;
            }

            return {
                status: "success",
                midtransData: responseData,
            };
        } catch (error) {
            throw new Error(
                "error at updateStatusBasedOnMidtransResponse : " +
                    error.message
            );
        }
    }
}

module.exports = TransactionServices;
