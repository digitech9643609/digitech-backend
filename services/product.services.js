const { Product } = require("../models");

class ProductServices {
    constructor() {}

    async createNewMultipleProduct({ products }) {
        try {
            const productsCreated = await Product.bulkCreate(products);

            return productsCreated;
        } catch (error) {
            throw new Error(
                "error at createNewMultipleProduct " + error.message
            );
        }
    }

    async createNewProduct({ name, price }) {
        try {
            const productCreated = await Product.create({ name, price });

            if (!productCreated) return { code: 40 };

            return productCreated;
        } catch (error) {
            throw new Error("error at createNewProduct : " + error.message);
        }
    }

    async getAllProduct() {
        try {
            const allProducts = await Product.findAll();

            return allProducts;
        } catch (error) {
            throw new Error("error at getAllProduct : " + error.message);
        }
    }

    async getProductsById({ product_ids }) {
        try {
            const productsData = await Product.findAll({
                where: {
                    id: product_ids,
                },
                attributes: ["id", "name", "price"],
            });

            return productsData;
        } catch (error) {
            throw new Error("error at validateProductIds : " + error.message);
        }
    }
}

module.exports = ProductServices;
