const {
    productService,
    transactionService,
} = require("../services/index.service");
const generateString = require("../utils/stringGenerator");
const {
    MIDTRANS_SERVER_KEY,
    PENDING_PAYMENT,
} = require("../utils/constantVariables");

module.exports = {
    createTransaction: async (req, res, next) => {
        try {
            const { products, customer_name, customer_email } = req.body;

            const product_ids = products.map((product) => product.id);

            const productsData = await productService.getProductsById({
                product_ids,
            });

            productsData.forEach((product) => {
                const productFromRequest = products.find(
                    (productFromRequest) => productFromRequest.id === product.id
                );
                product.quantity = productFromRequest.quantity;
            });

            const transaction_id = `ISC-${generateString(4)}-${generateString(
                8
            )}`;
            const gross_amount = productsData.reduce(
                (acc, product) => acc + product.quantity * product.price,
                0
            );

            const authString = btoa(`${MIDTRANS_SERVER_KEY}:`);

            const payload = {
                transaction_details: {
                    order_id: transaction_id,
                    gross_amount,
                },
                item_details: productsData.map((product) => ({
                    id: product.id,
                    name: product.name,
                    price: product.price,
                    quantity: product.quantity,
                })),
                customer_details: {
                    first_name: customer_name,
                    email: customer_email,
                },
            };

            const response = await fetch(
                `${process.env.MIDTRANS_APP_URL}/snap/v1/transactions`,
                {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                        Accept: "application/json",
                        Authorization: `Basic ${authString}`,
                    },
                    body: JSON.stringify(payload),
                }
            );

            console.log("response", response);

            const data = await response.json();

            if (response.status !== 201) {
                return res.status(400).json({
                    status: "failed",
                    message: "error occurred while creating transaction",
                    data: {
                        productsData,
                        payload,
                        data,
                        url: process.env.MIDTRANS_APP_URL,
                    },
                });
            }

            await Promise.all([
                transactionService.createTransaction({
                    transaction_id,
                    gross_amount,
                    customer_name,
                    customer_email,
                    snap_token: data.token,
                    snap_redirect_url: data.redirect_url,
                }),
                transactionService.createTransactionItems({
                    products: productsData,
                    transaction_id,
                }),
            ]);

            return res.status(201).json({
                status: "success",
                message: "transactions added successfully",
                data: {
                    id: transaction_id,
                    status: PENDING_PAYMENT,
                    customer_name,
                    customer_email,
                    products: productsData,
                    snap_token: data.token,
                    snap_redirect_url: data.redirect_url,
                },
            });
        } catch (error) {
            return res.status(500).json({
                status: "error",
                message: "internal server error",
                error: error.message,
            });
        }
    },

    getTransactionById: async (req, res, next) => {
        try {
            const { transaction_id } = req.query;

            const transactionData = await transactionService.getTransactionById(
                {
                    transaction_id,
                }
            );

            return res.status(200).json({
                status: "success",
                message: "get transaction by id",
                data: transactionData,
            });
        } catch (error) {
            return res.status(500).json({
                status: "error",
                message: "internal server error",
                error: error.message,
            });
        }
    },

    transactionNotificationFromMidtrans: async (req, res, next) => {
        try {
            const data = req.body;

            transactionService
                .getTransactionById({ transaction_id: data.order_id })
                .then((transaction) => {
                    if (transaction) {
                        transactionService
                            .updateStatusBasedOnMidtransResponse(
                                transaction.Tid,
                                data
                            )
                            .then((result) => {
                                console.log(
                                    "============ RESULT ============="
                                );
                                console.log("result", result);
                            })
                            .catch((error) =>
                                console.log(
                                    "error at update status midtrans ",
                                    error
                                )
                            );
                    }
                })
                .catch((error) =>
                    console.log("error att transaction Notification : ", error)
                );

            return res.status(200).json({
                status: "success",
                message: "OK",
            });
        } catch (error) {
            return res.status(500).json({
                status: "error",
                message: "internal server error",
                error: error.message,
            });
        }
    },
};
