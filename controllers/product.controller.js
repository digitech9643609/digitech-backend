const ProductServices = require("../services/product.services");

//create instance
const productService = new ProductServices();

module.exports = {
    getAllProduct: async (req, res, next) => {
        try {
            const allProducts = await productService.getAllProduct();

            return res.status(200).json({
                status: "success",
                message: "success get all product",
                data: allProducts,
            });
        } catch (error) {
            return res.status(500).json({
                status: "error",
                message: "internal server error",
                error: error.message,
            });
        }
    },

    createProduct: async (req, res, next) => {
        try {
            const { name, price } = req.body;

            const createdProduct = await productService.createNewProduct({
                name,
                price,
            });

            if (createdProduct.code === 40)
                return res.status(200).json({
                    status: "failed",
                    message: "failed to create product",
                });

            return res.status(200).json({
                status: "success",
                message: "product created successfully",
                data: createdProduct,
            });
        } catch (error) {
            return res.status(500).json({
                status: "error",
                message: "internal server error",
                error: error.message,
            });
        }
    },

    createNewMultipleProducts: async (req, res, next) => {
        try {
            const { products } = req.body;

            if (!Array.isArray(products)) {
                return res.status(400).json({
                    status: "failed",
                    message: "products should be an array",
                });
            }

            const productsCreated =
                await productService.createNewMultipleProduct({ products });

            return res.status(201).json({
                status: "success",
                message: "all new products added successfully",
                data: productsCreated,
            });
        } catch (error) {
            return res.status(500).json({
                status: "error",
                message: "internal server error",
                error: error.message,
            });
        }
    },
};
