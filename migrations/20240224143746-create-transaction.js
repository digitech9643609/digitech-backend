"use strict";
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable("Transactions", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            Tid: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            total: {
                type: Sequelize.INTEGER,
            },
            status: {
                type: Sequelize.STRING,
            },
            customer_name: {
                type: Sequelize.STRING,
            },
            customer_email: {
                type: Sequelize.STRING,
            },
            snap_token: {
                type: Sequelize.STRING,
            },
            snap_redirect_url: {
                type: Sequelize.STRING,
            },
            payment_method: {
                type: Sequelize.STRING,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
        });
    },
    async down(queryInterface, Sequelize) {
        await queryInterface.dropTable("Transactions");
    },
};
